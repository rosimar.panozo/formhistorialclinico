const {model, Schema} = require('mongoose');

const newPacienteSchema = new Schema({
    nombre:{
        type: String,
    },
    edad:{
        type: Number,
    },
    sexo:{
        type: String,
    },
    ocupacion:{
        type: String,
    },
    procedencia:{
        type: String,
    },
    estadoc:{
        type: String,
    }
});

module.exports = model('Paciente', newPacienteSchema);