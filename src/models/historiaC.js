const {model, Schema}= require('mongoose');

const newHistorySchema = new Schema({
    paciente:{
        type:String,
    },
    motivo:{
        type: String,
    },
    enfermedad:{
        type: String,
    },
    examenFisico:{
        type: String,
    },
    impresionDi:{
        type: String,
    },
    conducta:{
        type: String,
    },
    fecha:{
        type: Date,
    },
    numhc:{
        type: Number,
    },
})

module.exports = model('HistoriaClinica', newHistorySchema)