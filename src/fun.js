const options = { format: 'A4', base: './assets' }
const fileName = `PDFDemo-${Date.now()}`
const output = `hc.pdf`
let pdfResult = await pdfUtil.toPDF('views/index.html', options, output)