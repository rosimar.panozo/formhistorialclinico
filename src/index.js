const{app,BrowserWindow,Menu, ipcMain} = require('electron');
const url = require('url');
const path = require('path');

const HistoriaClinica = require('./models/historiaC');
const Paciente = require('./models/paciente');

if(process.env.NODE_ENV !== 'production'){
    require('electron-reload')(__dirname,{
        electron: path.join(__dirname, '../node_modules','.bin', 'electron')
    });
}

let mainWindow;

function createWindow(){
    mainWindow = new BrowserWindow({
        maxWidth:1000,
        minWidth:900,
        minHeight:700,
        maxHeight:700,
        webPreferences:{
            nodeIntegration:true,
            contextIsolation: false
        }
    })
    mainWindow.loadFile('./views/index.html')
   // const mainMenu= Menu.buildFromTemplate(templateMenu);
 //   Menu.setApplicationMenu(mainMenu);
}


const templateMenu =[{
    label:'File',
    submenu:[
        {
            label: 'Nuevo Formulario',
            accelerator: 'Ctrl+n',
            click(){
                alert('nuevo Formulario');
            }
        }
    ]
}];

function createNewForm(){
    new BrowserWindow({
        width:400,
        height:300,
        title:'Crear nuevo Formulario'
    })
}

//escucha las historias clinicas y guarda dato
ipcMain.on('new-history',async (e,args)=>{ //ipcMain escucha/recibe el evento new-history y (evento, args)
    const newHistoryC = new HistoriaClinica(args); //se crea un nuevo obj 
    const historySaved = await newHistoryC.save(); //guarda el objeto
    console.log(historySaved);
    e.reply('new-history-created', JSON.stringify(historySaved)); //
});

ipcMain.on('new-paciente',async (e,args)=>{ //ipcMain escucha/recibe el evento new-paciente y (evento, args)
    const newPaciente = new Paciente(args); //se crea un nuevo obj 
    const pacienteSaved = await newPaciente.save(); //guarda el objeto
    console.log(pacienteSaved);
    e.reply('new-paciente-created', JSON.stringify(pacienteSaved)); //
});

//obtiene los pacientes
ipcMain.on('get-pacientes', async(e,args)=>{ 
    const pacientes=  await Paciente.find();
    console.log(pacientes);
    e.reply('get-pacientes',JSON.stringify(pacientes));
})

module.exports={createWindow};