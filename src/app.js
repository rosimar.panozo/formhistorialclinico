const { ipcRenderer } = require("electron");

const formHC = document.querySelector("#formHistorial");
const formPaciente = document.querySelector("#formPaciente");

const btnNuevo = document.querySelector("#btnNuevo");
const btnGuardar = document.querySelector("#btnGuardar");

const nombreP = document.querySelector("#nombre");
const edadP = document.querySelector("#edad");
const sexoP = document.querySelector("#sexo");
const procedenciaP = document.querySelector("#procedenciaRes");
const estadoCP = document.querySelector("#estadocRes");
const ocupacionP = document.querySelector("#ocupacionRes");

const fechaH = document.querySelector("#fecha");
const nhcH = document.querySelector("#nhc");
const motivoH = document.querySelector("#motivo");
const eactualH = document.querySelector("#eactual");
const efisicoH = document.querySelector("#efisico");
const impresionH = document.querySelector("#impresion");
const conductaH = document.querySelector("#conducta");

var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today = now.getFullYear()+"-"+(month)+"-"+(day);

fechaH.value = today;
btnNuevo.addEventListener("click", (e) => {
  e.preventDefault();
  formHC.reset();
  formPaciente.reset();
});
btnGuardar.addEventListener("click", (e) => {
  const historiaClinica = {
    paciente: nombreP.value,
    motivo: motivoH.value,
    enfermedad: eactualH.value,
    examenFisico: efisicoH.value,
    impresionDi: impresionH.value,
    conducta: conductaH.value,
    fecha: fechaH.value,
    numhc: nhcH.value,
  };
  const datospaciente = {
    nombre: nombreP.value,
    edad: edadP.value,
    sexo: sexoP.value,
    ocupacion: ocupacionP.value,
    procedencia: procedenciaP.value,
    estadoc: estadoCP.value,
  };
  ipcRenderer.send("new-history", historiaClinica); // ipcRenderer envia el dato del evento
  ipcRenderer.send("new-paciente", datospaciente);
});
//
ipcRenderer.on("new-history-created", (e, args) => {
  try {
    const historySaved = JSON.parse(args);
    console.log(historySaved);
    alert("Guardado exitosamente");
  } catch (error) {
    alert("Error al guardar");
  }
});

ipcRenderer.on("new-paciente-created", (e, args) => {
  const pacienteSaved = JSON.parse(args);
  console.log(pacienteSaved);
});

ipcRenderer.send("get-pacientes");

ipcRenderer.on("get-pacientes", (e, args) => {
  const pacientes = JSON.parse(args);
});
