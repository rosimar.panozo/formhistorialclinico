const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/electrondb',{
    useUnifiedTopology: true,
    useNewUrlParser: true

})
.then(db=> console.log('DB is connected'))
.catch(er=> console.log(err));