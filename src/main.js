const {createWindow} = require('./index');
const{app}= require('electron');
require('./database');
app.whenReady().then(createWindow);
